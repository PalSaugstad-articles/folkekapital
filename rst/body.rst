Balansen
--------

Et sentralt begrep innenfor bank er *balansen*, som viser hvordan forvaltningskapitalen er fordelt.
Balansen har en aktiva-side og en passiva-side.
Disse må være like store.
Passiva-siden viser hvor bankens kapital kommer ifra.
En viktig post her er bank-kundenes innskudd.
Aktiva-siden viser hvordan banken har plassert denne kapitalen.
En viktig post her er utlån til publikum.
Dersom vi summerer alle bankers og kredittforetak sine balanser, får vi en total-oversikt over alle innskudd og utlån,
samt alle de andre postene forvaltningskapitalen består av.
Her vil summen av alle innkudd omlag tilsvare pengemengden som det føres statistikk over av Statistisk Sentralbyrå.
Tilsvarende vil alle utlån tilsvare kredittindikatoren.

.. raw:: html

    <figure>
        <img src="img/bankenes-betalingsbalanse.png" alt="Bankenes balanse">
        <figcaption>Fig. 1.<br>
        Bankenes betalingsbalanse. Aktiva og passiva. Norskeide banker og OMF-kredittforetak per 30. juni 2014.</figcaption>
    </figure>

Forvaltingskapitalen til bankene vil stort sett alltid øke med noen prosent hvert år.
Dette er en konsekvens av inflasjon, samt økonomisk vekst og/eller økning i folketallet.

Direkte årsaker til forvaltningskapitalens endringer
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Banker kan skape og destruere penger.
Når banken yter et nytt lån, vil aktivasiden øke med like mye som størrelsen på den nye lånet.
I tillegg øker passivasiden med like mye, fordi kunden som tar opp lånet vil få pengene overført til en konto i banken.
Det motsatte skjer når kunden betaler tilbake (deler av) lånet.
Penger fra kundens konto forsvinner og blir avregnet mot lånet, som også blir tilsvarende mye mindre.

Balansen må alltid ha like stor aktiva- og passiva-side
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

En annen grunn til at balansen endres, kan være at en lånekunde ikke er i stand til å betale tilbake lånet sitt.
Banken må i så fall avskrive lånet.
Dette reduserer aktivasiden på balansen.
Banken må da finne ressurser på passivasiden som den må avskrive slik at aktiva- og passiva-sidene igjen blir like store.
De forskjellige postene på passivasiden tas etter prioritet.
Først kommer egenkapitalen.
Når banken må avskrive egenkapital, betyr dette i praksis at de som sitter på egenkapitalbevis vil tape penger.
Hvis det ikke finnes mer egenkapital, må neste post delvis avskrives.

Forenklet modell
----------------

I en forenklet modell av banker, kan vi innføre kun tre poster, utlån, innskudd og egenkapital.
Vi har da altså kun utlån til publikum på aktiva-siden, og innskudd fra publikum pluss egenkapital på passiva-siden.
Det kreves at bankene skal ha en viss prosent-del av passiva-siden som ansvarlig kapital,
som i hovedsak består av egenkapitalen.
Den kan skaffes til veie ved at bankene selger egenkapitalbevis til publikum.
En annen mulighet er at bankene setter tilside litt av overskuddet sitt som egenkapital.

.. raw:: html

    <figure>
        <img src="img/forenklet.png" alt="Forenklet balanse">
        <figcaption>Fig. 2. Forenklet balanse</figcaption>
    </figure>


Sannsynligheten for mislighold øker når kredittindikatoren øker i forhold til pengemengden
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

I figur 2 ser vi lett hva som skjer hvis kunder misligholder lån.
Banken må kompensere dette ved å avskrive deler av egenkapitalen (ansvarlig kapital) slik at aktiva- og passiva-sidene blir like store.
Det kan synes å være fornuftig at egenkapitalen er en forholdsvis stor del av passiva-siden,
for, jo mer egenkapital, jo mer "solid" er banken.
(Jo mer er banken i stand til å rydde opp i situasjoner der mange lån blir misligholdt.)

Men stopp en hal. Større egenkapital-krav fortrenger jo innskudd på passiva-siden.
For når publikum kjøper egenkapitalbevis, bytter den ut noe av pengeformuen sin med mindre likvid formue,
og pengene tilsvarende beløpet det ble kjøpt egenkapitalbevis for, forsvinner.
Så hvis egenkapitalkravet var 99%, så ville bare 1% av passivasiden vært innskudd fra publikum,
mens 100% av aktiva-siden fortsatt ville vært utlån til publikum.
*Kredittindikatoren ville altså blitt 100 ganger så stor som pengemengden.*

Egenkapital-kravet må være passe stort
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Det er altså åpenbart at egenkapital-kravet må være passelig stort, hverken for lite eller for stort.
Hvis det er for lite vil vi kunne risikere at publikums innskudd vil lide ved mislighold av lån,
fordi dette er det eneste banken rår over etter at all egenkapital er avskrevet.
Hvis det er for stort vil forholdet mellom kreditt og innskudd bli svært ugunstig,
det blir for mye gjeld i forhold til innskudd. Nå kan man innvende at dette forholdet ikke er noe problem,
fordi publikums samlede finansielle formue likevel er like stor som låne-mengden, forskjellen er bare
at denne formuen består av en større andel av egenkapitalbevis, og en mindre andel innskudd.
Sant nok, men i praksis, siden formuen er svært ujevnt fordelt hos publikum, så vil det bli vanskelig
for folk med stor gjeld å få tak i penger, siden de selv som regel ikke har noen formue i form av
egenkapitalbevis.

Folkekapital
------------

Hva så hvis vi innfører én obligatorisk post til i bankenes balanse, men denne gangen på aktiva-siden?
La meg kalle den for **Folkekapital**.
Da blir Ansvarlig kapital eller **Egenkapital** den kapitalen banken skaffer seg ved å selge egenkapitalbevis eller sette av deler av overkuddet,
og **Folkekapital** den kapitalen som banken har som krav at de må tilføre publikum ved å kjøpe folkekapital-obligasjoner.
Det kan for eksempel være en statlig eller halv-statlig organisasjon som selger folkekapital-obligasjonene.
Denne organsisjonen skal ha som oppgave å videreformidle disse pengene direkte til befolkningen,
f. eks. som en del av barnetrygden,
som jo deles ut uten behovsprøving til alle barnefamilier,
eller annen utbetaling som f. eks. borgerlønn.

.. raw:: html

    <figure>
        <img src="img/balanse_med_folkekapital.png" alt="Balanse med Folkekapital">
        <figcaption>Fig. 3. Balanse med Folkekapital</figcaption>
    </figure>

Folkekapital-obligasjoner skal i utgangspunktet være evigvarende og rentefrie.
Som beskrevet over skal forvaltingskapitalen alltid øke år for år, så det vil sjelden være behov for å innløse disse obligasjonene igjen.
Dersom en bank avvikles, kan disse obligasjonene kjøpes av/overdras til andre banker,
som jo også vil overta både lån og innskudd fra den banken som blir avviklet.

En ulempe for bankene er at de ikke kan tjene renter på disse obligasjonene.
Det betyr at de må kompensere ved å sette renten på vanlige lån noe høyere.

Hvor stor andel av aktiva-siden bør man kreve at er folkekapital?
Trolig er en prosent-andel tilsvarende egenkapitalkravet omtrent passe.
Legg merke til en ganske bra ting med denne obligasjonen: La oss (for regne-eksempelets skyld) si at kravet ble innført over natten,
fra 0% til 10% folkekapital. Da vil aktivasiden øke med 10%, åpenbart. Men også passiva-siden vil øke like mye, fordi
pengene som oppstår som følge av at bankene kjøper disse obligasjonene vil oppstå i form av nye innskudd fra publikum umiddelbart.
Hvis nå både egenkapital-kravet og folkekapital-kravet er 10%, vil både kredittmengden og pengemengden være 90% av forvaltningskapitalen,
altså like store.
Dagen før var jo kredittmengden 10% større enn pengemengden!

En mer pragmatisk måte å innføre folkekapitalen på er å gjøre det over endel år. For hvert år økes folkekapitalkravet med 0,5 prosent,
og så ser man an når det er best å stoppe.

Hva hvis vi f. eks setter egenkapital-kravet til 20% og folkekapitalkravet til 30% slik som angitt i figur 3?
Da vil pengemengden bli større enn kredittmengden,
noe som etter all sannsynlighet minsker sjansen for at lån blir misligholdt.

Folkekapital vil kunne ha en stabiliserende effekt på banksystemet, sannsynligheten for mislighold av lån
(antatt at alle andre parametere like) blir lavere dersom pengemengden er større enn kredittmengden.


Kilder
^^^^^^

* `Steinar Holden, Makroøkonomi, 2016
  <https://www.cappelendamm.no/_fagboker/makro%C3%B8konomi-steinar-holden-9788202474096>`_
  Fig. 1 er hentet fra side 320, Kapittel 13, Finansmarkedet.
* `Cultura Bank, Egenkapitalbevis - Bli medeier i Cultura Bank!, 2018
  <https://www.cultura.no/arkiv/pengevirke/egenkapitalbevis>`_
