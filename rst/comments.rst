---------------------------

===========
Kommentarer
===========

---------------------------

.. topic:: Tilleggsinformasjon (2019-07-06 21:37):

Folkekapital
er et ment som et konsept som forbedrer pengesystemet i samme retning som forslaget til
`Positive Money
<https://positivemoney.org/>`_.
Med Folkekapital vil ikke bankene bli fratatt all rett til å skape penger (slik som Positive Money vil),
men de vil bli pålagt å utstede evigvarende rentefrie penger vha. obligasjoner utstedt av en offentlig kontrollert organisasjon.
Funksjonen til denne organisasjonen blir antakelig på linje med Positve Money sin komité som skal bestemme nivået på pengemengden.
I Folkekapital-forslaget har jeg ikke det skarpe skillet mellom pengeskaping og pengeformidling slik som Positive Money vil ha.
Jeg tror det er mer realistisk å få gjennomslag for et Folkekapital enn for forslaget til Positive Money,
og det de har felles er evnen til å kunne fungere i et klima der vi har nullvekst eller svak negativ vekst.


Kommentaren over er klippet fra
`Gaveøkonomi for klimaets skyld
<gaveokonomi>`_,
nærmere bestemt
 `her
<gaveokonomi#C2019-06-30-19-00>`_.
